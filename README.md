# CSRF Token Handling #

## Summary ##

Whilst working on RESTful web services (a client to SAP HANA) I had difficulty with getting CSRF working.

This file is critical to letting SAP HANA clients connect.